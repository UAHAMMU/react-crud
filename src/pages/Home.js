import { async } from '@firebase/util';
import { getDocs,collection, doc, deleteDoc } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import TodoList from '../components/TodoList';
import { useUserAuth } from "../context/UserAuthContext";
import { db } from '../Firebaseconfig';
import { Oval } from  'react-loader-spinner'


const Home = ({list,setList}) => {


    const { logOut, user } = useUserAuth();
    const [list2, setList2]=useState([]);
    const todoCollectionRef= collection(db, "todo");
    const [isloading,setIsloading]= useState(false);

  

  useEffect(()=>{
   
   
  const getList= async () => {
    
   setIsloading(true);
   const data= await getDocs(todoCollectionRef);
   setList2(data.docs.map((doc)=> ({ ...doc.data(),id:doc.id})));  
   setIsloading(false);
  

  }
   getList();
   
  },[]);  
   
   const handleLogout= async () => {

    try{
        await logOut();
    }catch(err){
      console.log(err.message);  
    }
   }

   const itemDelete = async (id) => {
    
    const listDoc= doc(db,"todo",id );

    setIsloading(true);
    await deleteDoc(listDoc);
    const data= await getDocs(todoCollectionRef);
    setList2(data.docs.map((doc)=> ({ ...doc.data(),id:doc.id})));
    setIsloading(false);

   }
  
   const handleDelete =(id)=> {
       const newList= list.filter(item => item.id !== id)
       setList(newList);
   }
   
   const handleCheckboxchange =(id) => { 
     
        const newTodoList=list2.map(item => {
            console.log(item.id)
            if(item.id === id)
                return { ...item,done:!item.done}
            return item;    
        });
        setList2(newTodoList);
   }

    return (
        <div className='home'>
            <h2>{user?.displayName}</h2>
            
             <h2>All the Todos</h2> 
              {isloading && <Oval color="#00BFFF" height={50} width={50} />}
              {list2.length>0?list2.map((item,i) => <TodoList item={item} key={i}  handleDelete={itemDelete} handlechange={handleCheckboxchange}/> ):<p align="center"> No To do to show! </p> }
         <button onClick={handleLogout}> logout</button>
        </div>
    );

};


export default Home;