
import { signInWithPopup } from "firebase/auth";
import { useState } from "react";
import { auth,provider } from "../Firebaseconfig";
import {Alert} from 'react-bootstrap'
import {useUserAuth} from "../context/UserAuthContext"
import { async } from "@firebase/util";
import { Link, useNavigate } from "react-router-dom";
import GoogleButton from "react-google-button"

const Login = () => {
     
    const [error,setError] = useState("");
     const { googleSignIn } = useUserAuth();
    let navigate=useNavigate();


    const handleGoogleSignIn = async(e) => {
       
        e.preventDefault();

        try {
          await googleSignIn();
          navigate("/home");
     

        }catch(err)
        {
            setError(err.message);
        }

    }; 


    return (   
        <div className="loginPage">
            {error && <Alert variant="danger">{error}</Alert>}
            <div className="create">
            <GoogleButton className="g-btn" type="dark" onClick={handleGoogleSignIn} />
            </div>
           
        </div>
    );
} 
 
export default Login;