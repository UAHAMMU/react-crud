import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { addDoc, collection,updateDoc,getDocs,doc} from "firebase/firestore";
import { db } from "../Firebaseconfig";
import { Oval } from  'react-loader-spinner'

const EditTodoDetails = ({ list, setList }) => {
    const [data, setData] = useState({ task: '', des: '', id: '', done: '' });
    const [list2, setList2]=useState([]);
    const [isloading,setIsloading]= useState(false);
    const { id } = useParams();
    const todoCollectionRef= collection(db, "todo");
   
    useEffect(()=>{
   
   
        const getList= async () => {
          
         setIsloading(true);
         const data= await getDocs(todoCollectionRef);
         setList2(data.docs.map((doc)=> ({ ...doc.data(),id:doc.id}))); 
         //const listData = list2?.filter((value, i) => { return value?.id === id }); 
         //console.log("this",list2.filter((value,i) => { return value?.id === id } ));
        console.log(list2);
         setIsloading(false);
        
      
        }
         getList();
         },[id]);  
    
  
    let navigate = useNavigate();
    

    const EidtTodo2 =  async (e) => {
        setIsloading(true);
        console.log( "the id is",id);
        e.preventDefault();
        const listDoc= doc(db,"todo",id );
        const newField= {task: data.task, des: data.des}
        await updateDoc(listDoc,newField);
        setIsloading(false);
        navigate("/home");

       // const data= await getDocs(todoCollectionRef);
    }
    
     
    const EditTodo = (e) => {
        e.preventDefault();
        const newUpdatedTodo = list.map((item) => {
            if (item?.id === parseInt(id)) {
                return { ...item, task: data.task, des: data.des }
            }
            return item;
        });
        setList(newUpdatedTodo);
        navigate("/home");
        console.log("updated Todo", newUpdatedTodo);


    }


    console.log("id" ,Math.floor(Math.random() * 100));

    return (
        <div className="create">
            <h2>Edit ToDo{id}</h2>
            {isloading && 
        <div style={{ display: 'flex',  justifyContent:'center', alignItems:'center', height: '20vh'  }} >
             <Oval  color="#00BFFF" height={50} width={50}/> </div> }
            <form>
                <label>Task:</label>
                <input
                    type="text"
                    required
                    value={data?.task}
                    onChange={(e) => setData({ ...data, task: e.target.value })}
                />
                <label>Task Descripition:</label>
                <textarea
                    required
                    value={data?.des}
                    onChange={(e) => setData({ ...data, des: e?.target?.value })}
                ></textarea>
                <button onClick={EidtTodo2}>Edit Todo</button>
            </form>
        </div>
    );
}

export default EditTodoDetails;