import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { addDoc, collection } from "firebase/firestore";
import { db } from "../Firebaseconfig";
import { useUserAuth } from "../context/UserAuthContext";
import { Oval } from  'react-loader-spinner'

const CreateTodo = ({list,setList}) => {

    const [task, setTask] = useState('');
    const [des, setDes] = useState('');
    const [isloading,setIsloading]= useState(false);
    const { logOut, user } = useUserAuth();


      
    const todoCollectionRef= collection(db, "todo");

    let navigate= useNavigate();

    const CreateList= async (e) =>  {
      e.preventDefault();
      setIsloading(true);
      await addDoc(todoCollectionRef, {
        task,
        des, 
        owner:{
             name:user?.displayName,
             id:user?.uid
      },
      done:false
    
    });
       
      setList([...list,{task,des,id: Math.floor(Math.random() * 100),done:false}]);
      setIsloading(false);
      navigate("/home");

    }
 
    return (
      <div className="create">
        <h2>Add a New Todo</h2>
        {isloading && 
        
          <div style={{
            display: 'flex',  justifyContent:'center', alignItems:'center', height: '20vh'
          }} > <Oval  color="#00BFFF" height={50} width={50}/> </div>
        
        }
        <form>
          <label>Task:</label>
          <input 
            type="text" 
            required 
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
          <label>Task Descripition:</label>
          <textarea
            required
            value={des}
            onChange={(e) => setDes(e.target.value)}
          ></textarea>
          <button onClick={CreateList}>Add Todo</button>
        </form> </div>
    );
}
 
export default CreateTodo;