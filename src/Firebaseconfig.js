// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getAuth, GoogleAuthProvider } from "firebase/auth";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA8FUbq_FPwtmhlgygBXisKHufR-o0jl1A",
  authDomain: "to-item-list.firebaseapp.com",
  projectId: "to-item-list",
  storageBucket: "to-item-list.appspot.com",
  messagingSenderId: "607714515418",
  appId: "1:607714515418:web:8a78c36e6d9408f241c8e3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db= getFirestore(app);
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider();

