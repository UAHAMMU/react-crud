import Navbar from './components/Navbar';
import Home from './pages/Home';
import CreateTodo from './pages/CreateTodo';
import { BrowserRouter as Router, Routes,Route,Link} from "react-router-dom"
import React,{useState} from 'react';
import Login from './pages/Login';
import EditTodoDetails from './pages/EditTodoDetails';
import { UserAuthContextProvider } from './context/UserAuthContext';
import ProtectedRoute from './components/ProtectedRoute';
function App() {
  const [list, setList] = useState([
    { task: 'Ui Desinging', des: 'Design according to user requirements', id: 1,done:false },
    { task: 'Code Implementation', des: 'Maximum try to Develop ', id: 2, done:false },
    { task: 'Code Testing', des: 'Something interesting ............ in Pearson', id: 3, done:false }
]);



  return (
    <UserAuthContextProvider>
    <Router>
    <div className="App">
      <Navbar/>
      <div className='content'> 
            <Routes>
            <Route
                path="/home"
                element={
                  <ProtectedRoute>
                    <Home setList={setList} list={list}/>
                  </ProtectedRoute>
                }
              />
              <Route path='/create'   
              element= {
              <ProtectedRoute>
                <CreateTodo setList={setList} list={list}/>
              </ProtectedRoute>
              }
              
              />
            
              <Route path="/item/:id" element={<EditTodoDetails setList={setList} list={list} />}/>
              <Route path="/" element={<Login/>}/>
            </Routes>        
      </div>
    </div>
    </Router>
    </UserAuthContextProvider>
  );
}

export default App;
