import { createContext, useContext, useEffect, useState } from 'react';
import {
    signOut,
    GoogleAuthProvider,
    signInWithPopup,
    onAuthStateChanged
} from "firebase/auth";
import { auth } from '../Firebaseconfig';


const userAuthContext = createContext();

export function UserAuthContextProvider  ({ children })  {
    const [user, setUser] = useState("");
   
    function googleSignIn() {
    
        const googleAuthProvider = new GoogleAuthProvider();
        return signInWithPopup(auth,googleAuthProvider);

    }
    function logOut() {
        return signOut(auth);
      }

    useEffect(() => {
       // clean up  when ever component unmount, prevent LISTING FORM THIS  call....
       const unsubscribe = onAuthStateChanged(auth, (currentUser)=> {
               setUser(currentUser);
        });
        return () => {
            unsubscribe();
        }

    },[]);
useEffect(()=>{
    if(user===null){
        logOut()
    }
},[user])
    return <userAuthContext.Provider value={{user,setUser,googleSignIn,logOut}}>{ children }</userAuthContext.Provider>
}

 export function useUserAuth()  {
    return useContext(userAuthContext);
}
 
