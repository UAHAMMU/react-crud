import { Link } from "react-router-dom";
import React,{useState} from "react";
const TodoList = ({item,handleDelete,handlechange}) => {
   
   const[checked,setChecked]=useState(item?.checked)
    const handleCheckboxchange = () => {
          if(item.done)
          return null;
          handlechange(item.id)
    }
  
    return ( 
        <div className="item-list">
            
                <div className="item-preview">
                    <h2 style={ item.done?{textDecoration:"line-through"}:null} >{item.task}</h2>
                    <p>Descripition: {item.des}</p>
                    <input type="checkbox" checked={checked} style={{ margin: "0 50px" }} onChange={(e) =>{
                        console.log(e?.target?.value)
                        handleCheckboxchange()}} ></input>
                    <Link to={`/item/${item.id}`}>
                     <button>Edit</button>
                     </Link>
                    
                    <button onClick={()=>handleDelete(item.id)}>Delete Item </button>

                </div>
            
        </div>
     );
}
 
export default TodoList;