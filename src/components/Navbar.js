import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useUserAuth } from "../context/UserAuthContext";
// import { async } from '@firebase/util';

const Navbar = () => {
    const { logOut, user,setUser } = useUserAuth();
   
    const handleLogout = async() =>{
        
        try{
            console.log("gonna set user to null")
        setUser(null)
        }catch(err){
          console.log(err);  
        }

    }
    var buttonText = "LOGOUT";
    return (
        <div>
            <nav className="navbar">
                <h1>THE TODO ITEM LIST</h1>
                <div className="links">
                    <Link to="/home">Home</Link>
                    <Link to="/create" style={{
                        color: 'white',
                        backgroundColor: '#f1356d',
                        borderRadius: '8px'
                    }}>New Item</Link>
                   
                    {user?.displayName? <button className='logout' onClick={(e)=>{e.preventDefault();handleLogout()}}>{buttonText}</button> : <Link style={{
                       
                    }} to="/">LOGIN</Link>}
                    
                </div>
            </nav>
        </div>
    );
};



export default Navbar;